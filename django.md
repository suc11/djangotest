# smartscigs

## 创建数据库表
```
python manage.py makemigrations
python manage.py migrate

Reporter.objects.all()
CREATE TABLE person (
personid integer primary key autoincrement, 
name varchar(20))

SQLiteOpenHelper
SQLiteDatabase
Cursor

```

## 域类型
```
models.CharField(max_length=70)
models.DateField()
models.TextField()
models.ForeignKey(Reporter, on_delete=models.CASCADE)

NULL,
INTEGER,
REAL(浮点数),
TEXT(字符串文本)
BLOB(二进制对象)
```

## 管理界面
```
admin.site.register(models.Article)
```

## 设计你的 URL

## 视图View

## 模板
1. 变量被双花括号括起来
2. 模板过滤器 |
3. 模板继承: {% extends "base.html" %}

## 安装
```
python3 -m venv Django-env
Django-env\Scripts\activate.bat
deactivate

pip install Django
pip uninstall Django

import django
print(django.get_version())
```

## tutorial
```
django-admin startproject mysite        #项目默认目录: 第一个mysite

python manage.py runserver
python manage.py runserver 0.0.0.0:8000

python manage.py startapp polls

path(route, view, kwargs, name)

python manage.py migrate
sqlite3 db.sqlite3
.tables
.quit

python manage.py makemigrations polls   #生成迁移文件
python manage.py sqlmigrate polls 0001
python manage.py check
python manage.py migrate                #创建新定义的模型的数据表

python manage.py shell
from polls.models import Choice, Question
Question.objects.all()
from django.utils import timezone
q = Question(question_text="What's new?", pub_date=timezone.now())
q.save()
q.id
q.question_text
q.pub_date
Question.objects.filter(question_text__startswith='What')       #__
Question.objects.get(Question.pub_date.year==current_year)      #???
Question.objects.get(pk=1)

q.choice_set.create(choice_text='Not much', votes=0)            #Foreign Key
q.choice_set.all()
q.choice_set.count()

Choice.objects.filter(question__pub_date__year=current_year)    #Use double underscores to separate relationships
c = q.choice_set.filter(choice_text__startswith='Just hacking')
c.delete()
```
```
# 管理页面
python manage.py createsuperuser

```

## 复习1-4 部分
```
python3 -m venv Django-env
Django-env\Scripts\activate.bat

pip install Django

django-admin startproject smartscigs                   #生成网站
python manage.py runserver

python manage.py startapp write_article                #生成app

# 编写第一个视图  : views.py
# 创建 URLconf   : write_article/urls.py                #url to 上面视图
# 网站urls.py链接: smartscigs/urls.py                   #url to app
```
```
# 创建系统数据库
python manage.py migrate

# 创建模型: models.py                                   #数据库表
# 激活模型: settings.INSTALLED_APPS                     #Add app to 网站
# 创建App数据库
python manage.py makemigrations write_article          #生成迁移文件
python manage.py migrate                               #应用数据库迁移

python manage.py shell
Question.objects.all()                                  #objects库表里面所有记录
Question.objects.filter(question_text__startswith='What')   #SQL里面用__双分隔, 外面Python用.
# 访问外键表方法1, choice_set
q = Question.objects.get(pk=1)
q.choice_set.all()                                       #Question.objects.all()
c = q.choice_set.create(choice_text='Not much', votes=0) #有外键的表的创建方法1
c.question                                               #访问外键
c.delete()
#有外键的表的创建方法2
q = Question.objects.get(pk=1)
c = Choice(question=q, choice_text="direct createChoice", votes=0)
```
```
# 管理页面
python manage.py createsuperuser
# 加入投票应用: polls/admin.py
```
```
#View 表单处理
# 决定共有几个页面, 修改urls.py
# 写出页面的模板, *.html
# 使用通用视图: views.py
```