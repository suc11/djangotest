from django.db import models

# Create your models here.
from django.db import models
import datetime
from django.utils import timezone
from django.contrib import admin

class Question(models.Model):
    question_text = models.CharField(max_length=200)    # 实例变量, 列名
    pub_date = models.DateTimeField('date published')   # 人类友好的名字

    def __str__(self):
        return self.question_text

    @admin.display(
        boolean=True,
        ordering='pub_date',
        description='Published recently?',
    )
    def was_published_recently(self):
        now = timezone.now()
        return now - datetime.timedelta(days=1) <= self.pub_date <= now


class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)

    def __str__(self):
        return self.choice_text
    

def getModel(tableName):
    class MyClassMetaclass(models.base.ModelBase):
        def __new__(cls, name, bases, attrs):
            name += tableName
            return models.base.ModelBase.__new__(cls, name, bases, attrs)
    
    class MyClass(models.Model):
        __metaclass__ = MyClassMetaclass
        
        english_name= models.CharField(max_length=10, primary_key=True)
        native_name = models.CharField(max_length=10)
        def __str__(self):
            return self.english_name
    
        class Meta:
            db_table = tableName
        
    return MyClass

china = getModel('China')
#canada = getModel('Canada')
