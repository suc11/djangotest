from django.urls import path

from . import views
from .models import getModel

app_name = 'polls'
urlpatterns = [
    # ex: /polls/
    path('', views.IndexView.as_view(), name='index'),
    
    # ex: /polls/5/
    path('<int:pk>/', views.DetailView.as_view(), name='detail'),
    
    # ex: /polls/5/results/
    path('<int:pk>/results/', views.ResultsView.as_view(), name='results'),
    
    # ex: /polls/5/vote/
    path('<int:question_id>/vote/', views.vote, name='vote'),
]

if False:
    newClass = getModel('China')
    china = newClass(english_name='China', native_name='zhongguo')
    china.save()

    #newClass = getModel('Canada')
    #canada = newClass(english_name='Canada', native_name='canada')
    #canada.save()

##########Test code
#python manage.py makemigrations
#python manage.py migrate
#python manage.py createsuperuser
#python manage.py runserver

#python manage.py shell

#from polls.models import getModel

#from django.apps import apps
#tables = [m._meta.db_table for c in apps.get_app_configs() for m in c.get_models()]
#tables