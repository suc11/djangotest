from django.contrib import admin

# Register your models here.
from .models import Question, Choice, getModel 


#class ChoiceInline(admin.StackedInline):
class ChoiceInline(admin.TabularInline):
    model = Choice
    extra = 3


class QuestionAdmin(admin.ModelAdmin):
    #分组
    fieldsets = [
        (None,               {'fields': ['question_text']}),
        ('Date information', {'fields': ['pub_date'], 'classes': ['collapse']}),
    ]
    inlines = [ChoiceInline]

    list_filter = ['pub_date']
    list_display = ('question_text', 'pub_date', 'was_published_recently')

    search_fields = ['question_text']

admin.site.register(Question, QuestionAdmin)
#admin.site.register( getModel('China') )
#admin.site.register( getModel('Canada') )