from django.shortcuts import render, get_object_or_404

# Create your views here.
from django.http import HttpResponse, HttpResponseRedirect
from django.http import Http404

from django.template import loader
from django.urls import reverse
from django.views import generic

from .models import Question, Choice

class IndexView(generic.ListView):                  #显示一个对象列表
    template_name = 'write_article/index.html'              #<app name>/<model name>_list.html 的默认模板
    context_object_name = 'latest_question_list'    #自动生成的 context 变量

    def get_queryset(self):
        """Return the last five published questions."""
        return Question.objects.order_by('-pub_date')[:5]


class DetailView(generic.DetailView):               #显示一个特定类型对象的详细信息页面
    model = Question
    template_name = 'write_article/detail.html'             #默认情况下, 使用 <app name>/<model name>_detail.html 的模板


class ResultsView(generic.DetailView):              #确保 results 视图和 detail 视图在渲染时具有不同的外观
    model = Question
    template_name = 'write_article/results.html'


def vote(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    try:
        selected_choice = question.choice_set.get(pk=request.POST['choice'])
    except (KeyError, Choice.DoesNotExist):
        # Redisplay the question voting form.
        return render(request, 'write_article/detail.html', {
            'question': question,
            'error_message': "You didn't select a choice.",
        })
    else:
        selected_choice.votes += 1      # 使用 F() 避免竞争条件
        selected_choice.save()
        # Always return an HttpResponseRedirect after successfully dealing
        # with POST data. This prevents data from being posted twice if a
        # user hits the Back button.
        return HttpResponseRedirect(reverse('write_article:results', args=(question.id,)))